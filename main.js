document.addEventListener('DOMContentLoaded', () => {

  const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0)
  
  if ($navbarBurgers.length > 0) {
  
    $navbarBurgers.forEach( el => {
      el.addEventListener('click', () => {
  
        const target = el.dataset.target;
        const $target = document.getElementById(target)
  
        el.classList.toggle('is-active')
        $target.classList.toggle('is-active')
  
      });
    });
  }
    
    const $myForm = document.querySelector('#siteContact')

    const handleSubmit = (e) => {

      e.preventDefault()
      
      let formData = new FormData($myForm)
      fetch('/', {
        method: 'POST',
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
        body: new URLSearchParams(formData).toString()
      })
      .then( (res) =>{ 
        if(!res.ok){
          throw({err: 'We get an error sending the message, try it later 🙇🏻'})
        }
        alert('Your message has been send. I will try to response you soon. Thank you! 🥳')
      })
      .catch((error) => alert(error.err))
      .finally(() => $myForm.reset())
    }
  
    $myForm.addEventListener('submit', handleSubmit)

  });